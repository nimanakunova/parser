import requests
from bs4 import BeautifulSoup as bs
import csv

HEADERS = {'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) '
                         'Gecko/20100101 Firefox/80.0',
           'accept': '*/*'}

def get_link(inn):
    link = f'https://register.minjust.gov.kg/register/SearchAction.seam?city=' \
           f'&founder=&chief=&house=&room=&okpo=&number=&baseBusiness=' \
           f'&fullnameRu=&street=&district=&tin={inn}&logic=and&region=&cid=2784677'
    return link


def get_html(url, params=''):
    r = requests.get(url=url, headers=HEADERS, params=params)
    return r


def get_content(html):
    soup = bs(html, 'html.parser')
    items = soup.find('td', class_="rich-table-cell",
                      id="searchActionForm:searchAction:0:j_id148")
    real_name = items.get_text()
    return real_name


def modify_real_name(string):
    position_index = string.find('Руководитель')
    return string[:position_index]


def main():
    with open('newparsercsv.csv', 'w', encoding="windows-1251") \
            as new_file:
        fieldnames = ['account',
                      'client_name',
                      'jur_name',
                      'INN',
                      'real_name']
        writer = csv.DictWriter(new_file,
                                fieldnames=fieldnames,
                                delimiter=';',
                                quoting=csv.QUOTE_NONE,
                                escapechar='',
                                quotechar=''
                                )
        writer.writeheader()

        with open('parsercsv.csv', 'r', encoding="windows-1251") as file:
            records = csv.DictReader(file, delimiter=';')
            for record in records:
                client_name = record['client_name']
                jur_name = record['jur_name']
                if (client_name.find(jur_name)) == -1:
                    inn = record['INN']
                    url = get_link(inn)
                    html = get_html(url)
                    name = get_content(html.text)
                    record['real_name'] = modify_real_name(name)
                    writer.writerow(record)


if __name__ == '__main__':
    main()
