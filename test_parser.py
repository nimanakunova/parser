import unittest
import parser

URL = f'https://register.minjust.gov.kg/register/' \
                           f'SearchAction.seam?city=&founder=&chief=&house=' \
                           f'&room=&okpo=&number=&baseBusiness=&fullnameRu=' \
                           f'&street=&district=&tin=00901199710162&logic=' \
                           f'and&region=&cid=2784677'


class TestCase(unittest.TestCase):

    def test_get_link(self):
        inn = '00901199710162'
        expectation_link = f'https://register.minjust.gov.kg/register/' \
                           f'SearchAction.seam?city=&founder=&chief=&house=' \
                           f'&room=&okpo=&number=&baseBusiness=&fullnameRu=' \
                           f'&street=&district=&tin=00901199710162&logic=' \
                           f'and&region=&cid=2784677'

        link = parser.get_link(inn)
        self.assertEqual(expectation_link, link)


    def test_get_content(self):
        with open('minjust.html', 'r') as html_file:
            get_page = html_file.read()
        real_name = parser.get_content(html=get_page)
        expectation_real_name = 'Общество с ограниченной ответственностью ' \
                                '"Международный бизнес центр научно-технической' \
                                ' информации"Руководитель: Сатыбалдиев Зарыл Усупович '

        self.assertEqual(expectation_real_name, real_name)

    def test_modify_name(self):
        data = 'Общество с ограниченной ответственностью ' \
                                 '"Международный бизнес центр научно-технической' \
                                 ' информации"Руководитель: Сатыбалдиев Зарыл Усупович '

        expectation_string = 'Общество с ограниченной ответственностью ' \
                                 '"Международный бизнес центр научно-технической' \
                                 ' информации"'

        string = parser.modify_real_name(data)
        self.assertEqual(expectation_string, string)







